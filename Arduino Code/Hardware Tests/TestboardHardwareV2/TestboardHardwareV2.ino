#include "AssdetectorButton.h"
#include "RangeFinder.h"

/*****[Collection of Assdetectors]*****/
AssDetectorButton buttox1(18,2);
AssDetectorButton buttox2(19,3);
AssDetectorButton buttox3(20,4);
AssDetectorButton buttox4(21,5);

/*****[Collection of legDetectors]*****/
RangeFinder legDetector1(14, 50, 6);
RangeFinder legDetector2(15, 50, 7);
RangeFinder legDetector3(16, 50, 8);
RangeFinder legDetector4(17, 50, 9);

/*****[Payload that needs to be sent to the ledcontroller]*****/
const int payLoadLength = 10;
uint8_t payLoad[payLoadLength] = {
	
	'*', 	//Start of message
	0,		// 0 or 1 -- someone is sitting down on the outmost left spot
	0,		// 0 or 1 -- someone is sitting down on the middle left spot
	0,		// 0 or 1 -- someone is sitting down on the middle right spot
	0,		// 0 or 1 -- someone is sitting down on the outmost right spot
	0,		// 0 to 3 -- Rangefinder detection level outmost left sensor
	0,		// 0 to 3 -- Rangefinder detection level middle left sensor
	0,		// 0 to 3 -- Rangefinder detection level middle right sensor
	0,		// 0 to 3 -- Rangefinder detection level outmost right sensor
	'$'		//End of message
};

/*****[Timer Variables]*****/
unsigned int messageInterval = 100;
unsigned long previousTimeMessage = 0;

unsigned long previousTimeRecalibrate = 0;
unsigned int recalibrateInterval = 600000; //10 minutes

void setup(){

	Serial.begin(19200);
	Serial1.begin(19200);

}

void loop(){
	unsigned long currentTime = millis();

	legDetector1.Update();
	legDetector2.Update();
	legDetector3.Update();
	legDetector4.Update();

	//check for the presence of ass on the detector
	buttox1.Update();
	buttox2.Update();
	buttox3.Update();
	buttox4.Update();

	composePayload();
	
	if((unsigned long)(currentTime - previousTimeMessage) >= messageInterval){
		//If the intervaltime has passed
		sendPayload();
		//Save the current time for the next interval
		previousTimeMessage = currentTime;
	}
	
	/*****[Debug part]*****/
	/*Serial.print(buttox1.ReturnState());
	Serial.print("\t");
	Serial.print(buttox2.ReturnState());
	Serial.print("\t");
	Serial.print(buttox3.ReturnState());
	Serial.print("\t");
	Serial.println(buttox4.ReturnState());
	*/
	/*
	for(int i=1; i<payLoadLength-1; i++){
		//debug the stuff send over serial
		Serial.print(payLoad[i]);
		Serial.print("\t");
	}
	Serial.println();
	*/

}

void  composePayload(){

	payLoad[1] = buttox1.ReturnState();
	payLoad[2] = buttox2.ReturnState();
	payLoad[3] = buttox3.ReturnState();
	payLoad[4] = buttox4.ReturnState();
	payLoad[5] = legDetector1.ReturnState();
	payLoad[6] = legDetector2.ReturnState();
	payLoad[7] = legDetector3.ReturnState();
	payLoad[8] = legDetector4.ReturnState();
}

void sendPayload(){

	for(int i=0; i<payLoadLength; i++){
	   Serial1.write(payLoad[i]);
	}
}
